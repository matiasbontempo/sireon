<?php

require_once './scripts/include/openid.php';
require_once './scripts/include/functions.php';
require_once './scripts/include/db.php';

session_start();

$sitio = "http://sireon.url.ph";
//$sitio = "http://127.0.0.1/sireon";

if(!ini_get('date.timezone')) date_default_timezone_set('GMT');

try {

	$openid = new LightOpenID('sireon.url.ph');
	//$openid = new LightOpenID('127.0.0.1/sireon');

	if(!$openid->mode) {
	    if(isset($_GET['login'])) {
            $openid->identity = 'https://www.google.com/accounts/o8/id';
    		$openid->required = array('namePerson/first', 'namePerson/last', 'contact/email');
            header('Location: ' . $openid->authUrl());
	    }
    } elseif($openid->mode == 'cancel') {
        echo 'User has canceled authentication!';
    } else {

		unset($_SESSION['nombre']);
		unset($_SESSION['apellido']);
		unset($_SESSION['id']);
		unset($_SESSION['horas']);
		unset($_SESSION['admin']);
		unset($_SESSION['email']);

        $data = $openid->getAttributes();
        $email = $data['contact/email'];
        $apellido = $data['namePerson/last'];
        $nombre = $data['namePerson/first'];
        $identity = explode("=", $openid->identity);

		$murialdino = explode("@", $email);

        if ($murialdino[1] != "murialdo.edu.ar") {
			unset($_SESSION['nombre']);
			unset($_SESSION['apellido']);
			unset($_SESSION['id']);
			unset($_SESSION['horas']);
			unset($_SESSION['admin']);
			unset($_SESSION['email']);
			header('Location: https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue='+$sitio);
        	return false;
        }

        $sql = "SELECT id FROM docentes WHERE email = '".$email."'";
        $rs = mysql_query($sql) or die(mysql_error() . "<br />" . $sql);


        if (!mysql_num_rows($rs)) {
        	$sql = "INSERT INTO docentes (nombre, apellido, email, googleID) VALUES ('".$nombre."', '".$apellido."', '".$email."', '".$identity[1]."')";
        	$rs = mysql_query($sql) or die(mysql_error() . "<br />" . $sql);
		}

		$sql = "SELECT id, googleID, nombre, apellido, horas FROM docentes WHERE email = '".$email."'";
		$rs = mysql_query($sql) or die(mysql_error() . "<br />" . $sql);

		$resultados = mysql_fetch_array($rs);

		$_SESSION['id'] = $resultados['id'];
		$_SESSION['nombre'] = $resultados['nombre'];
		$_SESSION['apellido'] = $resultados['apellido'];
		$_SESSION['horas'] = $resultados['horas'];
		$_SESSION['email'] = $email;
		if ($resultados['horas'] == 9999) {
			$_SESSION['admin'] = "fuckyeah";
		}

    }
} catch(ErrorException $e) {
    echo $e->getMessage();
}

?>