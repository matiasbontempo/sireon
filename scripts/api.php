<?php

	require_once("./include/functions.php");
	require_once("./include/db.php");

	session_start();

	if(!ini_get('date.timezone')) date_default_timezone_set('GMT');

	$año = 2014;

	$hoy = getdate();

	$accion = isSet($_GET['accion']) ? $_GET['accion'] : false;

	$json = array(
		"status" => "ERROR",
		"data" => "",
		"extra" => "",
		"accion" => $accion
	);

	switch($accion) {
		case "login":
			Login();
			break;

		case "logout":
			Logout();
			break;

		case "consultarMes":
			ConsultarMes();
			break;

		case "consultarDia":
			ConsultarDia();
			break;

		case "actualizarHorario":
			ActualizarHorario();
			break;

		case "actualizarHorario2":
			ActualizarHorario2();
			break;

		case "actualizarComentario":
			ActualizarComentario();
			break;

		case "infoUsuario":
			InfoUsuario();
			break;

		case "nombreRecurso":
			NombreRecurso();
			break;

		case "enviarInforme":
			EnviarInforme();
			break;

		case "adminDiario":
			AdminDiario();
			break;

		case "adminValidar":
			AdminValidar();
			break;

		case "adminCancelar":
			AdminCancelar();
			break;

		case "adminEditar":
			AdminEditar();
			break;
	}

	// echo $json['data'];
	$json['data'] = $json['data'];
	echo json_encode($json);


	function Login() {
		// session_destroy();
		unset($_SESSION['nombre']);
		unset($_SESSION['apellido']);
		unset($_SESSION['id']);
		unset($_SESSION['horas']);
		unset($_SESSION['admin']);
		unset($_SESSION['email']);

		$sql = "SELECT id, nombre, apellido, horas, googleID FROM docentes WHERE CONCAT(nombre,apellido) = '".$_POST['user']."'";
		$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);

		if ($resultados = mysql_fetch_array($rs)) {
			if (!$resultados['googleID'] && $_POST['pass']) {
				$sql = "UPDATE docentes SET googleID = '".md5($_POST['pass'])."' WHERE CONCAT(nombre,apellido) = '".$_POST['user']."'";
				$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
				$_SESSION['nombre'] = $resultados['nombre'];
				$_SESSION['apellido'] = $resultados['apellido'];
				$_SESSION['id'] = $resultados['id'];
				$_SESSION['horas'] = $resultados['horas'];
				$_SESSION['email'] = "sireon@murialdo.edu.ar";
				if ($resultados['horas'] == 9999) {
					$_SESSION['admin'] = "fuckyeah";
					echo "ADMIN";
				} else {
					echo "OK";
				}
			} else {
				if ($resultados['googleID'] == md5($_POST['pass'])) {
					$_SESSION['nombre'] = $resultados['nombre'];
					$_SESSION['apellido'] = $resultados['apellido'];
					$_SESSION['id'] = $resultados['id'];
					$_SESSION['horas'] = $resultados['horas'];
				$_SESSION['email'] = "sireon@murialdo.edu.ar";
					if ($resultados['horas'] == 9999) {
						$_SESSION['admin'] = "fuckyeah";
						echo "ADMIN";
					} else {
						echo "OK";
					}
				} else {
					echo "INCORRECTO";
				}
			}
		}
	}
	function Logout() {
		global $json;
		unset($_SESSION['nombre']);
		unset($_SESSION['apellido']);
		unset($_SESSION['id']);
		unset($_SESSION['horas']);
		unset($_SESSION['email']);
		session_destroy();
		$json['status'] = "OK";
	}
	function ConsultarMes() {

		global $json, $con, $año;

		$sql = "SELECT DISTINCT dia, idEspacio FROM reservas WHERE mes = '".$_POST['numeroMes']."' and (idEspacio = '".$_POST['recurso']."' or idEspacio = '0') ORDER BY dia ASC";
		$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
		
		$resultados = mysql_fetch_array($rs);

		$numerito = date('w', mktime(0, 0, 0, $_POST['numeroMes'], 1, $año));

		if ($numerito > 0) {
			while ($numerito > 1) {
				$numerito--;
				$json['data'] .= ",0";
			}
		} else {
			$json['data'] .= ",0,0,0,0,0,0";
		}

		for ($i = 1; $i <= cal_days_in_month(CAL_GREGORIAN, $_POST['numeroMes'], $año); $i++) {
			if ($resultados['dia'] == $i) {
				if ($resultados['idEspacio'] == 0) {
					$json['data'] .= ",3";
				} else {
					$json['data'] .= ",2";
				}
				$resultados = mysql_fetch_array($rs);
			} else {
				$json['data'] .= ",1";
			}
		}

		$json['status'] = "OK";
	}
	function ConsultarDia() {
		
		global $con, $json;		

		$horasTurnos = array (
			"manana" => array(),
			"tarde" => array(),
			"noche" => array(),
			"comentario" => ""
		);

		if (isset($_SESSION['admin']) && $_SESSION['admin'] == "fuckyeah") {
			$sql = "SELECT comentario FROM reservas WHERE dia = '".$_POST['numeroDia']."' and mes = '".$_POST['numeroMes']."' and idEspacio = '".$_POST['recurso']."' and idProfe = '0'";
			$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
			$resultados = mysql_fetch_array($rs);
			$horasTurnos["comentario"] = $resultados[0];
		}

		// TURNO MAÑANA
		$sql = "SELECT hora, idProfe FROM reservas WHERE dia = '".$_POST['numeroDia']."' and mes = '".$_POST['numeroMes']."' and idEspacio = '".$_POST['recurso']."' and turno = '1' ORDER BY hora ASC";
		$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
		
		$turnoManana = array();

		while($resultados = mysql_fetch_array($rs)) {
			array_push($turnoManana, $resultados['hora']);
			if ($resultados['idProfe'] == $_SESSION['id']){
				array_push($turnoManana, 0);
			} else {
				array_push($turnoManana, convertirProfe($resultados['idProfe']));
			}
		}

		$horasTurnos["manana"] = $turnoManana;

		// TURNO TARDE
		$sql = "SELECT hora, idProfe FROM reservas WHERE dia = '".$_POST['numeroDia']."' and mes = '".$_POST['numeroMes']."' and idEspacio = '".$_POST['recurso']."' and turno = '2' ORDER BY hora ASC";
		$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);

		$turnoTarde = array();
		
		while($resultados = mysql_fetch_array($rs)) {
			array_push($turnoTarde, $resultados['hora']);
			if ($resultados['idProfe'] == $_SESSION['id']){
				array_push($turnoTarde, 0);
			} else {
				array_push($turnoTarde, convertirProfe($resultados['idProfe']));
			}
		}

		$horasTurnos["tarde"] = $turnoTarde;

		// TURNO NOCHE
		$sql = "SELECT hora, idProfe FROM reservas WHERE dia = '".$_POST['numeroDia']."' and mes = '".$_POST['numeroMes']."' and idEspacio = '".$_POST['recurso']."' and turno = '3' ORDER BY hora ASC";
		$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);

		$turnoNoche = array();
		
		while($resultados = mysql_fetch_array($rs)) {
			array_push($turnoNoche, $resultados['hora']);
			if ($resultados['idProfe'] == $_SESSION['id']){
				array_push($turnoNoche, 0);
			} else {
				array_push($turnoNoche, convertirProfe($resultados['idProfe']));
			}
		}

		$horasTurnos["noche"] = $turnoNoche;

		$json['data'] .= json_encode($horasTurnos);
		$json["status"] = "OK";
	}
	function ActualizarHorario() {

		global $con, $json;

		datalog();

		if (!isset($_SESSION['admin']) || $_SESSION['admin'] != "fuckyeah") {

			$sql = "SELECT id FROM reservas WHERE dia = '".$_POST['numeroDia']."' and mes = '".$_POST['numeroMes']."' and hora = '".$_POST['hora']."' and turno = '".$_POST['turno']."' and idProfe = '".$_SESSION["id"]."'";
			$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);

			if (mysql_num_rows($rs)) {
				$json['status'] = "Error";
				$json['data'] .= "Registro Duplicado.";
				return false;
			}
		}

		$sql = "SELECT id FROM reservas WHERE idProfe = ".$_SESSION["id"];
		$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
		$tmp = mysql_num_rows($rs);
		$sql = "SELECT horas FROM docentes WHERE id = ".$_SESSION["id"];
		$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
		$resultados = mysql_fetch_array($rs);
		if ($resultados["horas"] - $tmp > 0 && $_POST['numeroMes'] <= $hoy['mon'] + 1) {
			$sql = "INSERT INTO reservas (hora, turno, dia, mes, idEspacio, idProfe) VALUES ('".$_POST['hora']."', '".$_POST['turno']."', '".$_POST['numeroDia']."', '".$_POST['numeroMes']."', '".$_POST['recurso']."','".$_SESSION['id']."')";
			$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
			
			if($rs) {
				$json['status'] = "OK";
				$json['data'] .= "OK";
			}
		} else {
			$json['data'] .= "Límite superado.";
		}
	}
	function ActualizarHorario2() {
		
		global $con, $json, $hoy;		

		datalog();

		$todosLosTurnos = "(";
		$paraSQL = "";

		$horaTurno = split(",", $_POST['horas']);

		for ($i = 0; $i < count($horaTurno); $i += 2) {
			$paraSQL .= "('".$horaTurno[$i+1]."', '".$horaTurno[$i]."', '".$_POST['numeroDia']."', '".$_POST['numeroMes']."', '".$_POST['recurso']."','".$_SESSION['id']."'), ";
			$todosLosTurnos .= "(hora = '".$horaTurno[$i+1]."' AND turno = '".$horaTurno[$i]."') OR ";
		}

		$paraSQL = substr($paraSQL, 0, -2);
		$todosLosTurnos = substr($todosLosTurnos, 0, -4);

		$todosLosTurnos .= ")";

		if (!isset($_SESSION['admin']) || $_SESSION['admin'] != "fuckyeah") {

			$sql = "SELECT id FROM reservas WHERE dia = '".$_POST['numeroDia']."' and mes = '".$_POST['numeroMes']."' and ".$todosLosTurnos." and idProfe = '".$_SESSION["id"]."'";
			$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);

			if (mysql_num_rows($rs)) {
				$json['status'] = "Error";
				$json['data'] = "Registro Duplicado.";
				echo json_encode($json);
				return false;
			}
		}

		$sql = "SELECT id FROM reservas WHERE idProfe = ".$_SESSION["id"];
		$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
		$tmp = mysql_num_rows($rs);
		$sql = "SELECT horas FROM docentes WHERE id = ".$_SESSION["id"];
		$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
		$resultados = mysql_fetch_array($rs);
		if ($resultados["horas"] - $tmp > 0 && $_POST['numeroMes'] <= $hoy['mon'] + 1) {
			$sql = "INSERT INTO reservas (hora, turno, dia, mes, idEspacio, idProfe) VALUES ".$paraSQL;
			$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
			
			if($rs) {
				$json['status'] = "OK";
				$json['data'] .= "OK";
			}
		} else {
			$json['data'] .= "Límite superado.";
		}
	}
	function ActualizarComentario() {

		global $con, $json;

		datalog();

		$sql = "SELECT comentario FROM reservas WHERE idProfe = '0' AND dia = '".$_POST['dia']."' AND mes = '".$_POST['mes']."' AND idEspacio = '".$_POST['espacio']."'";
		$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
		if (mysql_num_rows($rs)) {
			$sql = "UPDATE reservas SET comentario = '".htmlspecialchars($_POST['comentario'])."' WHERE idProfe = '0' AND dia = '".$_POST['dia']."' AND mes = '".$_POST['mes']."' AND idEspacio = '".$_POST['espacio']."'";
			$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
		} else {
			$sql = "INSERT INTO reservas (idProfe, dia, mes, idEspacio, comentario) VALUES ('0', '".$_POST['dia']."', '".$_POST['mes']."', '".$_POST['espacio']."', '".htmlspecialchars($_POST['comentario'])."')";
			$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
		}

		$json['data'] .= "OK";
		$json['status'] = "OK";
	}
	function InfoUsuario() {

		global $con, $json, $hoy;

		$sql = "SELECT dia, mes, turno, hora, idEspacio FROM reservas WHERE idProfe = '".$_SESSION['id']."' AND (mes > '".$hoy['mon']."' OR (mes = '".$hoy['mon']."' AND dia >= '".$hoy['mday']."')) AND hora != '0' AND validado = '0' ORDER BY mes ASC, dia ASC, turno ASC, idEspacio ASC, hora ASC";
		$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
		if (!$rs) {
			$json['data'] .= mysql_error() . "<br />" . $sql;
			die();
		}

		$_dia = 0;
		$_mes = 0;
		$_turno = 0;
		$_recurso = 0;

		while ($resultados = mysql_fetch_array($rs)) {

			if ($resultados['dia'] != $_dia || $resultados['mes'] != $_mes || $resultados['turno'] != $_turno || $resultados['idEspacio'] != $_recurso) {
				$json['data'] .= ";".$resultados['dia'].",".$resultados['mes'].",".$resultados['turno'].",".$resultados['idEspacio'].";";

				$_dia = $resultados['dia'];
				$_mes = $resultados['mes'];
				$_turno = $resultados['turno'];
				$_recurso = $resultados['idEspacio'];
			}

			$json['data'] .= " - ".$resultados['hora'];
		}

		$json['status'] = "OK";
	}
	function NombreRecurso() {
		global $con, $json;
		$json['data'] .= convertirRecurso($_POST['numeroRecurso']);
		$json['status'] = "OK";
	}
	function EnviarInforme() {

			global $json;

			datalog();
			
			$headers = "From: ".$_SESSION['email']."\r\n";
			$headers .= "Reply-To: ".$_SESSION['email']."\r\n";
			$headers .= "Return-Path: ".$_SESSION['email']."\r\n";
			$headers .= 'X-Mailer: PHP/' . phpversion();

			if (mail("matias.bontempo@murialdo.edu.ar",$_SESSION['apellido'].", ".$_SESSION['nombre']." (".$_SESSION['email'].")",$_POST["contenido"],$headers)) {
				$json['data'] .= "OK";
				$json['status'] = "OK";
			}
	}
	function AdminDiario() {

		if (!isset($_SESSION['admin']) || $_SESSION['admin'] != "fuckyeah") return;

		global $con, $json, $hoy;

		
		if(isSet($_POST['dia']) && isSet($_POST['mes'])) {
			$hoy['mday'] = $_POST['dia'];
			$hoy['mon'] = $_POST['mes'];
		}
		/*
		$json['data'] .= '<div id="menu">
					<div id="volver">< Volver</div>
					<div id="titulo" val="0">
						Administraci&oacute;n Diaria - '.$hoy['mday'].'/'.$hoy['mon'].'
						<div id="prev">></div>&nbsp;&nbsp;<div id="next"><</div>
					</div>
				</div>';
*/
		$json['data'] .= '<div id="menu" class="row">
								<div id="prev" class="col-xs-2 text-left"><a class="label label-info"><i class="fa fa-chevron-left"></i></a></div>
								<div id="nombreMes" class="col-xs-8 text-center h3" style="margin: 0px 0px 10px;">Administraci&oacute;n Diaria - '.$hoy['mday'].'/'.$hoy['mon'].'</div>
								<div id="next" class="col-xs-2 text-right"><a class="label label-info"><i class="fa fa-chevron-right"></i></a></div>
							</div>';



		// mysql_query('SET CHARACTER SET utf8');
		$sql = "SELECT idEspacio, comentario FROM reservas WHERE dia = '".$hoy['mday']."' and mes = '".$hoy['mon']."' and idProfe = '0' ORDER BY idEspacio ASC";
		$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
		
		$json['data'] .= "<textarea style='height: 100px; width: 100%; margin-bottom: 15px;'>";
		while ($resultados = mysql_fetch_array($rs)) {
			$json['data'] .= "//".convertirRecurso($resultados['idEspacio'])."\n";
			$json['data'] .= "    ".html_entity_decode($resultados['comentario'])."\n\n";
		}
		$json['data'] .= "</textarea><br />";


		$json['data'] .= '<div id="registros">';
		$espacios = array();
		$sql = "SELECT * FROM espacios ORDER BY id ASC";
		$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);


		while ($resultados = mysql_fetch_array($rs)) {
			$espacios[$resultados['id']] = Normalizar($resultados['nombre']);
			//$espacios[$resultados['id']] = $resultados['nombre'];
		}
		$sql = "SELECT id, turno, idEspacio, idProfe, hora, comentario, validado FROM reservas WHERE idProfe > '0' AND dia = '".$hoy['mday']."' AND mes = '".$hoy['mon']."' AND turno != '0' ORDER BY turno ASC, idEspacio ASC, idProfe ASC, hora ASC";
		$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);

		$_turno = 0;

		while ($resultados = mysql_fetch_array($rs)) {

			if ($resultados['validado'] == 1) {
				$deshabilitar = "disabled='disabled'";
				$registrOff = " transparente";
			} else{
				$deshabilitar = "";	
				$registrOff = "";
			} 

			if ($_turno != $resultados['turno']) {
				$json['data'] .= "<div id='".$resultados['turno']."' class='turnoTitulos'>".convertirTurnoCompleto($resultados['turno'])."</div>";
				$_turno = $resultados['turno'];
			}

			$json['data'] .= "<div turno='".$resultados['turno']."' id='".$resultados['id']."' class = 'registro".$registrOff."'><div>".$resultados['id']."-".convertirTurno($resultados['turno'])."</div><div>".convertirRecursoCorto($resultados['idEspacio'])."</div><div><b>".utf8_encode(convertirProfe($resultados['idProfe']))."</b>";
			if ($resultados['comentario']) $json['data'] .= " <i>(".$resultados['comentario'].")</i>";
			$json['data'] .= "</div><div>".$resultados['hora']."</div><div><input class='validar' type='button' ".$deshabilitar." value='Validar' /><input class='cancelar' type='button' ".$deshabilitar." value='Cancelar' /><input class='editar' type='button' ".$deshabilitar." value='Editar' /></div></div>";

		}

		$json['data'] .= "<br /><br /><br />";
		$json['status'] = "OK";
	}
	function AdminValidar() {

		if (!isset($_SESSION['admin']) || $_SESSION['admin'] != "fuckyeah") return;

		global $con, $json;

		datalog();

		$sql = "UPDATE reservas SET validado = '1' WHERE id = '".$_POST['id']."'";
		$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
		if ($rs) {
			$json['status'] = "OK";
			$json['data'] .= "OK";
		}
	}
	function AdminCancelar() {

		if (!isset($_SESSION['admin']) || $_SESSION['admin'] != "fuckyeah") return;

		global $con, $json;

		datalog();

		$sql = "DELETE FROM reservas WHERE id = '".$_POST['id']."'";
		$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
		if ($rs) {
			$json['status'] = "OK";
			$json['data'] .= "OK";
		}
	}
	function AdminEditar() {

		if (!isset($_SESSION['admin']) || $_SESSION['admin'] != "fuckyeah") return;

		global $con, $json;

		datalog();
		
		$sql = "UPDATE reservas SET idProfe = '".$_SESSION["id"]."', comentario = '".$_POST["nombre"]."' WHERE id = '".$_POST['id']."'";
		$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
		if ($rs) {
			$json['data'] .= "OK@".$_SESSION["apellido"]."@".$_SESSION["nombre"];
			$json['status'] = "OK";
		}
	}








	function convertirTurno ($numeroTurno) {
		switch ($numeroTurno) {
			case 1:
				return "M";
			break;
			case 2:
				return "T";
			break;
			case 3:
				return "V";
			break;
		}
	}
	function convertirTurnoCompleto ($numeroTurno) {
		switch ($numeroTurno) {
			case 1:
				return "Turno Mañana";
			break;
			case 2:
				return "Turno Tarde";
			break;
			case 3:
				return "Turno Vespertino";
			break;
		}
	}
	function convertirRecurso ($numeroRecurso) {

		global $con;

		mysql_query('SET CHARACTER SET utf8');
		$sqlFunct = "SELECT nombre FROM espacios WHERE id = '".$numeroRecurso."'";
		$rsFunct = mysql_query($sqlFunct, $con) or die(mysql_error() . "<br />" . $sqlFunct);

		if ($resultadosFunct = mysql_fetch_array($rsFunct)) {
			return $resultadosFunct['nombre'];
		} else {
			return "ERROR";
		}
	}
	function convertirRecursoCorto ($numeroRecurso) {

		global $con;

		mysql_query('SET CHARACTER SET utf8');
		$sqlFunct = "SELECT corto FROM espacios WHERE id = '".$numeroRecurso."'";
		$rsFunct = mysql_query($sqlFunct, $con) or die(mysql_error() . "<br />" . $sqlFunct);

		if ($resultadosFunct = mysql_fetch_array($rsFunct)) {
			return $resultadosFunct['corto'];
		} else {
			return "ERROR";
		}
	}
	function convertirProfe ($numeroProfe) {

		global $con;

		//mysql_query('SET CHARACTER SET utf8');
		$sqlFunct = "SELECT nombre, apellido FROM docentes WHERE id = '".$numeroProfe."'";
		$rsFunct = mysql_query($sqlFunct, $con) or die(mysql_error() . "<br />" . $sqlFunct);

		if ($resultadosFunct = mysql_fetch_array($rsFunct)) {
			return Normalizar($resultadosFunct['apellido'].", ".$resultadosFunct['nombre']);
		} else {
			return "ERROR";
		}
	}
	function datalog () {

		global $con;

		$valores = "";
		foreach($_POST as $key => $value) $valores .= "$key=$value;";
		$valores = substr($valores, 0, -1);

		if (!empty($_SERVER['HTTP_CLIENT_IP'])) $ip = $_SERVER['HTTP_CLIENT_IP'];
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else $ip = $_SERVER['REMOTE_ADDR'];

		$sql = "INSERT INTO logs (idProfe, ip, cmd, var, time) VALUES ('".$_SESSION['id']."','$ip','".$_GET['accion']."','$valores',(NOW() + INTERVAL 1 HOUR))";
		$rs = mysql_query($sql, $con) or die(mysql_error() . "<br />" . $sql);
	}
	function Normalizar($stripAccents) {

		return strtr($stripAccents,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
	}
?>