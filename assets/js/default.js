

var inSystem = false;



var elRecurso = 0;

var elRecursoID = 0;

var elDia = 0;

var elMes = 0;

var laPantalla = 0;

var elNombre = 0;

var elApellido = 0;



var saltarCalendario = false;



var debug = false;



var elAnio = 2014;

var esAdmin = false;





window.onbeforeunload = function() { if (inSystem == true) return "Est\xE1 a punto de salir del sistema."; };



$(document).ready( function() {



	// AUTO LOGIN DESDE GOOGLE

	if($("#usr").val()) {



		elNombre = $("#usr").val();

		elApellido = $("#pss").val();


		if (!localStorage.getItem("version")) {

			localStorage.setItem("version", $(".navbar-brand span").html());

		} else if (localStorage.getItem("version") != $(".navbar-brand span").html()) {

			alert("Se ha actualizado el sistema!\nInformar cualquier nuevo error que aparezca.\nMuchas Gracias!");

			localStorage.setItem("version", $(".navbar-brand span").html());

		}



		$("#usr").val('');

		$("#pss").val('');



		//$("#eventos #datos").html(info);



		if ($("#admin").length) esAdmin = true;



		inSystem = true;

		

		$(".login-container").hide()

		$("#main-container").show();

		$("#mainInfo").show();

		$("#calendario").hide();

		$("#horas").hide();

		$("#adminDia").hide();

		cargando(false);

		actualizarReservas();


	} else {

		cargando(false);

	}



	$(".btn-googleplus").click(function() {

		cargando(true);

	});



	$("#mostrarReservas").click(function() {

		hideAndShow("#mainInfo");

		cargando(false);

	});



	// MOSTRAR CALENDARIO DE UN RECURSO O PANEL DE ADMINISTRACION SI SE ESTA LOGUEADO

	$(document).on('click', '#recursos a', function() {

		mostrarMes(new Date().getMonth() + 1, $(this).attr('id'));

	});





	$("#mesAnterior").click(function() {

		mostrarMes(elMes - 1, elRecursoID);

	});



	$("#mesSiguiente").click(function() {

		mostrarMes(elMes + 1, elRecursoID);

	});





	// MOSTRAR REGISTROS DEL DIA SELECCIONADO

	$("#semanas").on('click', 'div', function() {

		if ($(this).css('cursor') == "pointer") {

			mostrarDia($(this).html(), elMes, elRecursoID);

		}

	});



	// CLICK EN RESERVA DEL MAIN

	$(document).on('click', '.fecha', function(){

		mostrarDia($(this).attr("dia"), $(this).attr("mes"), $(this).attr("recurso"));

		saltarCalendario = true;

	});



	$("#horas").on('click', '.btn-inverse', function() {

		if (saltarCalendario) {

			hideAndShow("#mainInfo");

			saltarCalendario = false;

		} else {

			hideAndShow("#calendario");

		}

	});



/*

	$("#calendario #volver").click(function(){

		$("#recursos").show();

		$("#calendario").hide();

	});



	$(document).on("click", "#adminDia #volver", function(){

		$("#recursos").show();

		$("#adminDia").hide();

	});



	$("#mostrarDia").on('click', '#volver', function() {

		if (saltarCalendario) {

			$("#recursos").show();

			saltarCalendario = false;

		} else {

			$("#calendario").show();

		}

		$("#mostrarDia").hide();

	});

*/

	$("#horas").on('click', '.btn-primary', function() {

		ds = elDia;

		ms = elMes;

		da = new Date().getDate();

		ma = new Date().getMonth() + 1;

		dl = limiteReservas(15).getDate();

		ml = limiteReservas(15).getMonth()+1;



		if (!esAdmin && (ms < ma) || (ds < da && ms == ma) || (dl < ds && ms == ml) || (ms > ml)) {

			alert("Esta fecha no puede ser reservada. No se pueden hacer registros con fechas superiores a 15 días desde hoy.");

			return true;

		}

		cargando(true);

		error = "";



		var horas = "";



		$('input:checked').each(function() {



			if ($(this).attr('turno')) {

				horas = horas + $(this).attr("turno") + "," + $(this).attr("hora") + ",";

				/*todoMal = actualizarHorario($(this).attr("hora"), $(this).attr("turno"), elDia, elMes, elRecursoID);

				if (todoMal) {

					error = "Error: " + todoMal;

				}*/

			}

		});



		horas = horas.slice(0,-1);

		actualizarHorario2(elDia, elMes, elRecursoID, horas, function() {});



		if($("#mostrarDia textarea").val()) {

			php("actualizarComentario", "mes="+ms+"&dia="+ds+"&espacio="+elRecursoID+"&comentario="+$("#mostrarDia textarea").val(), function(info) {

				if(info != "OK") alert("Error al actualizar el comentario: "+info);

			});

		}

		if(!error)	{

			mostrarMes(elMes, elRecursoID);

			cargando(true);

			actualizarReservas();

		} else {

			alert(error);

			cargando(false);

		}



	});





	$(document).on("click", ".turnoTitulos", function(){

		$('div[turno="'+$(this).attr("id")+'"]').slideToggle();

	});

	$(document).on("click", "#mostrarAdminDia", function() {
		mostrarAdmin();
	});


	$(document).on("click", "#prev", function(){

		var date = elAnio+"-"+elMes+"-"+elDia;

		var datum = new Date(date);

		datum.setDate(datum.getDate() - 1);	// ACA HAY QUE SUMAR PARA QUE RESTE. ES RARO. PUEDE SER POR  COMO ORDENA EL EL FLOAT: RIGHT

		mostrarAdmin(datum.getDate(), datum.getMonth() + 1);

	});



	$(document).on("click", "#next", function(){

		var date = elAnio+"-"+elMes+"-"+elDia;

		var datum = new Date(date);

		datum.setDate(datum.getDate() + 1);	// ACA HAY QUE RESTAR PARA QUE SUME. ES RARO. PUEDE SER POR  COMO ORDENA EL EL FLOAT: RIGHT

		mostrarAdmin(datum.getDate(), datum.getMonth() + 1);

	});



	$(document).on("click", ".registro input", function(){

		registro = $(this).parent().parent();

		/* alert(registro.attr("id") + " - " + $(this).val());*/



		cargando(true);

		switch ($(this).val()) {



			case "Validar":

				php("adminValidar", "id="+registro.attr("id"), function(data) {

					if (data == "OK") {

						registro.animate({ opacity: .25 });

						registro.find("input").each(function() {

							$(this).attr("disabled", "disabled");

						});

					} else {

						alert(data);

					}

					cargando(false);

				})



				break;



			case "Cancelar":

				if (confirm('Desea eliminar este registro?')) {

					php("adminCancelar", "id="+registro.attr("id"), function(data) {

						if (data == "OK") {

							registro.slideUp(function() {

								registro.remove();

							});

						} else alert(data);

					    cargando(false);

					});

				} else {

					cargando(false);

				}

				break;



			case "Editar":

				responsableCambio = prompt("Responsable del Cambio");

				docenteCambio = prompt("Docente");

				if (responsableCambio && docenteCambio) {

					php("adminEditar", "id="+registro.attr("id")+"&nombre="+responsableCambio+" - "+docenteCambio, function(data) {

				    	var dataRecieved = data.split('@');

						if (dataRecieved[0] == "OK") {

							registro.find("div:nth-child(3)").html(dataRecieved[1] + ", " + dataRecieved[2] + " <i>("+responsableCambio+" - "+docenteCambio+")</i>");

						}

						cargando(false);

					});

				} else {

					cargando(false);

				}

				break;



		}

	});







	$("#salir").click(function(){

		cargando(true);

		php("logout", "", function(data) {

			document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://sireon.url.ph"

		});

	});







	$("#salir2").click(function(){

		cargando(true);

		php("logout", "", function(data) {

			document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://sireon.url.ph"

		});

	});





	$("#informar").click(function(){

		cargando(true);

		$("#enviarInforme").show();

		$("#enviarInforme textarea").val('');

		$("#enviarInforme textarea").focus();

	});



	$("#enviar").click(function(){

		if ($("#enviarInforme textarea").attr("disabled") != "disabled") {

			$("#enviarInforme textarea").attr("disabled", "disabled");

			php("enviarInforme", "contenido="+$("#enviarInforme textarea").val(), function(data) {

				if (data == "OK") {

					alert("Gracias por su comentario!");

					$("#enviarInforme textarea").removeAttr("disabled");

					$("#enviarInforme").hide();

					if($("#eventos").css("position") === "fixed" && $("#eventos").css("right") == "0px") {

						$("#eventos").animate({right: "-75%", duration: 100});

					}

					cargando(false);

				} else {

					alert("Error al enviar el comentario.");

					$("#enviarInforme textarea").removeAttr("disabled");

					$("#enviarInforme").hide();

					if($("#eventos").css("position") === "fixed" && $("#eventos").css("right") == "0px") {

						$("#eventos").animate({right: "-75%", duration: 100});

					}

					cargando(false);

				}

			});

		}

	});



	$("#cancelar").click(function(){		

		cargando(false);

		if($("#eventos").css("position") === "fixed" && $("#eventos").css("right") == "0px") {

			$("#eventos").animate({right: "-75%", duration: 100});

		}

		$("#enviarInforme").hide();

	});



	$(".navbar-toggle").click(function(){

		if($(".navbar-side").css("right") == "-190px") {

			cargando(true);

			$(".navbar-side").animate({right: 0, duration: 100});

		} else {

			cargando(false);

			$(".navbar-side").animate({right: -190, duration: 100});

		}

	});



	$("#overlay").click(function(){

		if($(".navbar-side").css("right") == "0px") {

			cargando(false);

			$(".navbar-side").animate({right: -190, duration: 100});

		}

	});







});



function cargando(siono) {

	if (siono) {

		$("#overlay").show();

		//$("#overlay").fadeIn();

	} else {

		//$("#overlay").hide();

		$("#overlay").fadeOut();

	}

}



function hideAndShow (parte) {

	if($(parte).is(':hidden')) { 

		$("#siteContent").children().slideUp();

		$(parte).slideDown();

	}



	if($(".navbar-side").css("right") == "0px") {

		$(".navbar-side").animate({right: -190, duration: 100});

	}

}



function mostrarMes(numeroMes, recurso) {

	// Muestra el calendario del mes especifico del recurso indicado.

	cargando(true);

	elMes = numeroMes;

	elRecursoID = recurso;

	elRecurso = nombreRecurso(recurso);



	php("consultarMes", "numeroMes="+numeroMes+"&recurso="+recurso, function(data) {



		var estados = data.substring(1).split(',');

		var mostrarNumero = 0;



		$("#semanas > div").removeClass("cLibre");

		$("#semanas > div").removeClass("cParcial");

		$("#semanas > div").removeClass("cFeriado");

		$("#semanas > div").text("");



		nombreMes(parseInt(numeroMes));



		$("#semanas > div").each(function(index) {

			switch (estados[index]) {

				case "1":

					$(this).addClass("cLibre");

					mostrarNumero++;

					break;

				case "2":

					$(this).addClass("cParcial");

					mostrarNumero++;

					break;

				case "3":

					$(this).addClass("cFeriado");

					mostrarNumero++;

					break;

			}

			if (estados[index] == "1" || estados[index] == "2" || estados[index] == "3") $(this).text(mostrarNumero);

		});

		

		/*

		$("#semanas").empty();

		$("#semanas").html(data);

		*/



		$("#breadMain").html("Reservar");

		$("#breadRecurso").html(elRecurso);

		$("#breadFecha").html("");



		$("#breadRecurso").addClass("active");



		hideAndShow("#calendario");



		cargando(false);

	});

}



function mostrarDia(numeroDia, numeroMes, recurso) {



	cargando(true);

	elMes = numeroMes;

	elDia = numeroDia;

	elRecursoID = recurso;

	elRecurso = nombreRecurso(recurso);



	php("consultarDia", "numeroDia="+numeroDia+"&numeroMes="+numeroMes+"&recurso="+recurso, function(data) {



		//alert(data);

		$("input:checkbox").removeAttr("title").removeAttr("disabled").removeAttr("checked");

		hideAndShow("#horas");



		var obj = jQuery.parseJSON(data);



/*

		for (var i = 2; i <= 6; i++) {

			$("#manana div:nth-child("+i+")").removeClass();

			$("#manana div:nth-child("+i+")").addClass("free");

			$("#manana div:nth-child("+i+")").removeAttr("title");

			$("#manana div:nth-child("+i+")").children().removeAttr("disabled");

			$("#manana div:nth-child("+i+")").children().removeAttr("checked");



			$("#tarde div:nth-child("+i+")").removeClass();

			$("#tarde div:nth-child("+i+")").addClass("free");

			$("#tarde div:nth-child("+i+")").removeAttr("title");

			$("#tarde div:nth-child("+i+")").children().removeAttr("disabled");

			$("#tarde div:nth-child("+i+")").children().removeAttr("checked");



			$("#noche div:nth-child("+i+")").removeClass();

			$("#noche div:nth-child("+i+")").addClass("free");

			$("#noche div:nth-child("+i+")").removeAttr("title");

			$("#noche div:nth-child("+i+")").children().removeAttr("disabled");

			$("#noche div:nth-child("+i+")").children().removeAttr("checked");

		}

*/

		if (obj.manana[0]) {

			for (var i = 0; i < obj.manana.length; i = i+2) {

				if (obj.manana[i+1] != 0) {

					$("#manana div:nth-child("+(parseInt(obj.manana[i]) + 1)+")").children().children("input").attr("title", obj.manana[i+1]);

				} else {

					$("#manana div:nth-child("+(parseInt(obj.manana[i]) + 1)+")").children().children("input").prop("checked", true);

				}

				$("#manana div:nth-child("+(parseInt(obj.manana[i]) + 1)+")").children().children("input").attr("disabled", "disabled");

			}

		}



		if (obj.tarde[0]) {

			for (var i = 0; i < obj.tarde.length; i = i+2) {

				if (obj.tarde[i+1] != 0) {

					$("#tarde div:nth-child("+(parseInt(obj.tarde[i]) + 1)+")").children().children("input").attr("title", obj.tarde[i+1]);

				} else {

					$("#tarde div:nth-child("+(parseInt(obj.tarde[i]) + 1)+")").children().children("input").prop("checked", true);

				}

				$("#tarde div:nth-child("+(parseInt(obj.tarde[i]) + 1)+")").children().children("input").attr("disabled", "disabled");

			}

		}

/*

		$("#mostrarDia textarea").html("");

		$("#mostrarDia textarea").val('');

		if ($("#mostrarDia textarea").length > 0) {

			$("#mostrarDia textarea").html(obj.comentario);

			$("#mostrarDia textarea").val(obj.comentario);

		}

*/



		

		$("#mostrarDia #recurso").html("<b>" + nombreRecurso(parseInt(recurso)) + "</b> &#124; " + numeroDia + "-" + numeroMes);





		$("#breadMain").html("Reservar");

		$("#breadRecurso").html(elRecurso);

		$("#breadFecha").html(numeroDia + "/" + numeroMes + "/" + elAnio);



		$("#breadFecha").addClass("active");



		hideAndShow("#horas");



		cargando(false);

	});

}



function mostrarAdmin (dia, mes) {

	cargando(true);

	elDia = new Date().getDate();

	elMes = new Date().getMonth() + 1;

	cadena = "";

	if (dia && mes) {

		cadena = "dia="+dia+"&mes="+mes;

		elDia = dia;

		elMes = mes;

	}

	php("adminDiario", cadena, function(data) {

		$("#adminDia").html(data);

		hideAndShow("#adminDia");

		cargando(false);

	});

}



function mostrarStats() {

	alert("Hello World!");

}



function actualizarReservas() {



	php("infoUsuario", "", function(info) {



		var reservas = info.substring(1).split(';');



		$("#mainInfo").html("");

		if (reservas[0] == "" || reservas.length == 0) {
			$("#mainInfo").append("<div class='jumbotron white'><h1>Ooops!</h1><b>Aún no hay reservas realizadas.</b> Utilizá el panel de la izquierda para seleccionar un <b>recurso</b>. Luego, seleccioná una fecha en el <b>calendario</b> <span class='small'>(Debe ser una fecha dentro de las dos semanas siguientes)</span>. Por último, clickeá en los <b>intervalos horarios</b> deseados y hacé click en <b>aceptar</b>.</div>");
		} else {

			for (var i = 0; i < reservas.length; i = i+2) {

				var datos = reservas[i].split(',');
				var datosHoras = reservas[i+1].substring(3);

				$("#mainInfo").append('<div class="col-lg-4 col-sm-6 col-xs-12 fecha" dia="'+datos[0]+'" mes="'+datos[1]+'" recurso="'+datos[3]+'"><a href="#" class="tile-button btn btn-primary"><div class="tile-content-wrapper reserva"><i class="fa fa-calendar"></i><small>'+datos[0]+'/'+datos[1]+'/'+elAnio+'</small><h3>'+nombreRecurso(datos[3])+'</h3><h4>'+nombreTurno(datos[2])+'</h4><center>'+datosHoras+'</center></div></a></div>');

			}
		}

		/*$("#eventos #datos").html(info);

		cargando(false);*/

	});



}



function actualizarHorario(hora, turno, numeroDia, numeroMes, recurso) {

	// alert("hora="+hora+"&turno="+turno+"&numeroDia="+numeroDia+"&numeroMes="+numeroMes+"&recurso="+recurso);

	php("actualizarHorario", "hora="+hora+"&turno="+turno+"&numeroDia="+numeroDia+"&numeroMes="+numeroMes+"&recurso="+recurso, function(data) {

		if (data != "OK") {

			alert("Sireon Error: "+data);

			return data;

		} else {

			// alert("Sireon OK");

		}

	});

}



function actualizarHorario2(numeroDia, numeroMes, recurso, horas, queHacer) {

	// alert("hora="+hora+"&turno="+turno+"&numeroDia="+numeroDia+"&numeroMes="+numeroMes+"&recurso="+recurso);

	php("actualizarHorario2", "numeroDia="+numeroDia+"&numeroMes="+numeroMes+"&recurso="+recurso+"&horas="+horas, queHacer);

}



function php(accion, data, success) {



	if (debug) {

		$.ajax({

			type: "POST",

			url: "./scripts/api.php?accion="+accion,

			data: data,

			timeout: 10000,

			error: function(objeto, quepaso, otroobj){
				alert("<strong>Error: </strong> No se pudo conectar con el servidor.<br />"+quepaso+" "+otroobj);
				//console.log("%o", objeto);
				console.warn(objeto.responseText);
			},

			success: function(data) {

				alert(data);

				success(data);

			}

		});

	} else {

		$.ajax({

			type: "POST",

			url: "./scripts/api.php?accion="+accion,

			data: data,

			dataType: "json",

			timeout: 10000,

			error: function(objeto, quepaso, otroobj){
				alert("<strong>Error: </strong> No se pudo conectar con el servidor.<br />"+quepaso+" "+otroobj);
				//console.log("%o", objeto);
				console.warn(objeto.responseText);
			},

			success: function(data) {

				if (data.extra) alert(data.extra);

				if (data.status == "OK") {

					success(data.data);

				} else {

					alert("Ha ocurrido un error. Intente nuevamente.\n"+data.data)

				}

			}

		});

	}

}



function nombreRecurso(numeroRecurso) {

	return $('#'+numeroRecurso).html();

}



function nombreMes(numeroMes) {



	var nombreMes;



	switch (numeroMes) {

		case 1:

			nombreMes = "Enero";

		break;

		case 2:

			nombreMes = "Febrero";

		break;

		case 3:

			nombreMes = "Marzo";

		break;

		case 4:

			nombreMes = "Abril";

		break;

		case 5:

			nombreMes = "Mayo";

		break;

		case 6:

			nombreMes = "Junio";

		break;

		case 7:

			nombreMes = "Julio";

		break;

		case 8:

			nombreMes = "Agosto";

		break;

		case 9:

			nombreMes = "Septiembre";

		break;

		case 10:

			nombreMes = "Octubre";

		break;

		case 11:

			nombreMes = "Noviembre";

		break;

		case 12:

			nombreMes = "Diciembre";

		break;



		default:

			alert(numeroMes);

			nombreMes = "Valor Incorrecto";

		break;



	}



	$("#nombreMes").html(nombreMes);

}



function nombreTurno(turno) {

	switch (turno) {

		case "1":

			return "TURNO MAÑANA";

			break;

		case "2":

			return "TURNO TARDE";

			break;

	}

}



function limiteReservas(days) {

    var today = new Date();

    return new Date(today.getTime() + days*24*60*60*1000);

}



function mostrarRecursos() {

	$("#calendario").hide();

	$("#mostrarDia").hide();

	$("#adminDia").hide();

}