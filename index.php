<?php
require './scripts/include/google.php';
$version = "2.0";
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Sireon - Sistema de Reservas Online</title>

    <meta charset="utf-8">
    <meta name="mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<meta name="msapplication-tap-highlight" content="no">
	<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi">
    <meta name="description" content="Sistema de Reservas Online de Recursos del Instituto Leonardo Murialdo.">
    <meta name="author" content="Matias Bontempo">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/fonts.css">
	<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
	
	<!-- PAGE LEVEL PLUGINS STYLES -->

    <!-- Tc core CSS -->
	<link id="qstyle" rel="stylesheet" href="assets/css/themes/style.css">	
	<!--[if lte IE 8]>
		<link rel="stylesheet" href="assets/css/ie-fix.css" />
	<![endif]-->
	
	
    <!-- Add custom CSS here -->

	<!-- End custom CSS here -->
	
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="assets/css/default.css">
	
  </head>

  <body>

  	<div id="overlay"></div>

	<div id="wrapper">

		<div class="login-container">

			<div id="login-box" class="login-box visible">			
					
				<img src="assets/images/ILM.png" alt="logo" class="img-responsive"><br />

				<div class="social-or-login">
					<span class="text-primary"><b>Ingresar con</b> @murialdo.edu.ar</span>
				</div>
					
				<div class="space-4"></div>
				
				<div class="text-center" style="padding: 10px 0px 30px;">
					<a href='?login' class="btn btn-googleplus btn-sm btn-circle"><i class="fa fa-google-plus icon-only bigger-130"></i></a>
				</div>

				<input type="hidden" id="usr" value="<?php if(isset($_SESSION['nombre'])) echo $_SESSION['nombre']; ?>" />

			</div>

		</div>

		<div id="main-container" class="container" style="display: none;">


			<div class="nav navbar-top container" role="navigation">

				<div class="navbar-header">

					<button type="button" class="navbar-toggle">
						<i class="fa fa-bars"></i>
					</button>

					<div class="navbar-brand">
						<a href="index.html">
							<img src="assets/images/sireon.png" alt="logo" class="img-responsive">
						</a>
						<span>v<?php echo $version; ?></span>
					</div>

				</div>

				<div class="nav-top">
					<ul class="nav navbar-right">
						<li><a id="salir" href="#"><i class="fa fa-power-off"></i> Salir</a></li>
					</ul>
				</div>
				
			</div>

			<nav class="navbar-side" role="navigation">
				<div class="navbar-collapse sidebar-collapse">
				
					<ul class="nav side-nav">
							<li><h4>Hola <?php echo $_SESSION['nombre']; ?>!</h4></li>
							<li>
								<a id="mostrarReservas" href="#"><i class="fa fa-check-square-o"></i> Reservas</a>
								<a id="salir2" href="#"><i class="fa fa-power-off"></i> Salir</a>
							</li>
					</ul>
				
					<ul class="nav side-nav">
						
						<li><h4>Recursos</h4></li>
						
						<li id="recursos">
							<a id="1" href="#">Multimedios (Cine)</a>
							<a id="2" href="#">Video (Entrepiso Poli)</a>
							<a id="3" href="#">CRA (Ex Biblioteca)</a>
							<a id="4" href="#">Atrio</a>
							<a id="5" href="#">Proyector 1</a>
							<a id="6" href="#">Proyector 2</a>
							<a id="7" href="#">Proyector CRA</a>
							<a id="8" href="#">Sal&oacute;n de Actos</a>
						</li>

					</ul>

					<?php
						if (isset($_SESSION['admin']) && $_SESSION['admin'] == "fuckyeah") {
							echo '<ul class="nav side-nav">
										<li><h4>Admin</h4></li>
										<li>
											<a href="#" id="mostrarAdminDia"><i class="fa fa-check-square-o text-danger"></i> Reservas</a>
											<a href="#"><i class="fa fa-area-chart text-success"></i> Estad&iacute;sticas</a>
										</li>
								</ul>';
						}

					?>
					
				</div>
			</nav>

			<div id="page-wrapper">
					
				<div class="row">

					<div class="col-lg-12">

						<div class="breadcrumbs">

							<ul class="breadcrumb">
								<li id="breadMain">Recurso</li>
								<li id="breadRecurso"></li>
								<li id="breadFecha"></li>
							</ul>
							
							<div class="b-right">
								<ul><li><a id="informar" href="#" title=""><i class="fa fa-comments"></i></a></li></ul>
							</div>

						</div>
					</div>

				</div>	

				
				<div id="siteContent" class="row">

					<div id="mainInfo" class="col-xs-12">
			
					</div>



					<div id="calendario" class="col-xs-12">

						<div class="row well white">

							<div class="row">
								<div id="mesAnterior" class="col-xs-2 text-left"><a class="label label-info"><i class="fa fa-chevron-left"></i></a></div>
								<div id="nombreMes" class="col-xs-8 text-center h3" style="margin: 0px 0px 10px;">Enero</div>
								<div id="mesSiguiente" class="col-xs-2 text-right"><a class="label label-info"><i class="fa fa-chevron-right"></i></a></div>
							</div>

							<div class="portlet">
								<div id="fechas" class="portlet-heading dark text-center border-none">
									<div class="col-xs-2">Lunes</div>
									<div class="col-xs-2">Martes</div>
									<div class="col-xs-2">Miercoles</div>
									<div class="col-xs-2">Jueves</div>
									<div class="col-xs-2">Viernes</div>
									<div class="col-xs-2">Fin de Semana</div>
								</div>

								<div class="portlet-body no-padding" id="semanas">
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-1"></div>
									<div class="col-xs-1"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-1"></div>
									<div class="col-xs-1"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-1"></div>
									<div class="col-xs-1"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-1"></div>
									<div class="col-xs-1"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-2"></div>
									<div class="col-xs-1"></div>
									<div class="col-xs-1"></div>
								</div>
							</div>
						</div>
					</div>


					<div id="horas" class="col-xs-12">

						<div class="portlet text-center">

							<div class="portlet-heading dark">
								<div class="col-xs-2">Turno</div>
								<div class="col-xs-2">1ra</div>
								<div class="col-xs-2">2da</div>
								<div class="col-xs-2">3ra</div>
								<div class="col-xs-2">4ta</div>
								<div class="col-xs-2">5ta</div>
							</div>

							<div class="portlet-body">
								<div class="row horario">
									<div class="col-xs-2"></div>
									<div class="col-xs-2">7:30 - 8:30</div>
									<div class="col-xs-2">8:30 - 9:30</div>
									<div class="col-xs-2">9:40 - 10:40</div>
									<div class="col-xs-2">10:40 - 11:40</div>
									<div class="col-xs-2">11:45 - 12:45</div>
								</div>
								<div id="manana" class="row">
									<div class="col-xs-2">Mañana</div>
									<div class="col-xs-2">
										<label>
											<input turno="1" hora="1" name="switch-field-1" class="tc tc-switch tc-switch-6" type="checkbox" disabled="disabled" checked="checked" title="Matías, Bontempo" />
											<span class="labels"></span>
										</label>
									</div>
									<div class="col-xs-2">
										<label>
											<input turno="1" hora="2" name="switch-field-1" class="tc tc-switch tc-switch-6" type="checkbox" disabled="disabled" checked="checked" title="Matías, Bontempo" />
											<span class="labels"></span>
										</label>
									</div>
									<div class="col-xs-2">
										<label>
											<input turno="1" hora="3" name="switch-field-1" class="tc tc-switch tc-switch-6" type="checkbox" disabled="disabled" checked="checked" title="Matías, Bontempo" />
											<span class="labels"></span>
										</label>
									</div>
									<div class="col-xs-2">
										<label>
											<input turno="1" hora="4" name="switch-field-1" class="tc tc-switch tc-switch-6" type="checkbox" disabled="disabled" checked="checked" title="Matías, Bontempo" />
											<span class="labels"></span>
										</label>
									</div>
									<div class="col-xs-2">
										<label>
											<input turno="1" hora="5" name="switch-field-1" class="tc tc-switch tc-switch-6" type="checkbox" />
											<span class="labels"></span>
										</label>
									</div>
								</div>
								<div class="hr"></div>
								<div class="row horario">
									<div class="col-xs-2"></div>
									<div class="col-xs-2">13:25 - 14:25</div>
									<div class="col-xs-2">14:25 - 15:25</div>
									<div class="col-xs-2">15:40 - 16:40</div>
									<div class="col-xs-2">16:40 - 17:40</div>
									<div class="col-xs-2">17:45 - 18:45</div>
								</div>
								<div id="tarde" class="row">
									<div class="col-xs-2">Tarde</div>
									<div class="col-xs-2">
										<label>
											<input turno="2" hora="1" name="switch-field-1" class="tc tc-switch tc-switch-6" type="checkbox" disabled="disabled" checked="checked" title="Matías, Bontempo" />
											<span class="labels"></span>
										</label>
									</div>
									<div class="col-xs-2">
										<label>
											<input turno="2" hora="2" name="switch-field-1" class="tc tc-switch tc-switch-6" type="checkbox" disabled="disabled" checked="checked" title="Matías, Bontempo" />
											<span class="labels"></span>
										</label>
									</div>
									<div class="col-xs-2">
										<label>
											<input turno="2" hora="3" name="switch-field-1" class="tc tc-switch tc-switch-6" type="checkbox" disabled="disabled" checked="checked" title="Matías, Bontempo" />
											<span class="labels"></span>
										</label>
									</div>
									<div class="col-xs-2">
										<label>
											<input turno="2" hora="4" name="switch-field-1" class="tc tc-switch tc-switch-6" type="checkbox" disabled="disabled" checked="checked" title="Matías, Bontempo" />
											<span class="labels"></span>
										</label>
									</div>
									<div class="col-xs-2">
										<label>
											<input turno="2" hora="5" name="switch-field-1" class="tc tc-switch tc-switch-6" type="checkbox" />
											<span class="labels"></span>
										</label>
									</div>
								</div>
							</div>

						</div>

						<div class="btn-group btn-group-justified">
							<div class="btn-group">
								<button type="button" class="btn btn-primary">Aceptar</button>
							</div>
							<div class="btn-group">
								<button type="button" class="btn btn-inverse">Cancelar</button>
							</div>
						</div>

					</div>


					<div id="adminDia" class="col-xs-12 well white" style="padding: 15px;">

					</div>
				
			</div>
							
			<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Sireon</span> por <a xmlns:cc="http://creativecommons.org/ns#" href="http://www.matiasbontempo.com.ar" property="cc:attributionName" rel="cc:attributionURL">Matias Bontempo</a> se distribuye bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Licencia Creative Commons 4.0 Internacional</a>.
						<a class="pull-right" rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" id="licencia">
							<img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png">
  						</a> 
					</div>
				</div>
			</div>

		</div>  
	</div> 
	 
    <!-- core JavaScript -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<!-- <script src="assets/js/plugins/pace/pace.min.js"></script> -->
	
	<!-- PAGE LEVEL PLUGINS JS -->
	
    <!-- Themes Core Scripts -->	
	<script src="assets/js/main.js"></script>

	<script src="assets/js/default.js"></script>
	
	<!-- initial page level scripts for examples -->	
  </body>
</html>